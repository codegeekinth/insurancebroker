


const state = {
    authenStatus: false
};

const actions = {
    authen ({commit}, status){
        commit('setAuthenStatus', status);
    }

};


const mutations = {
    setAuthenStatus(state, status){
        state.authenStatus = status;
    }
};


export default {
    namespaced: true,
    state,
    actions,
    mutations
  };